import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataService {

  private url = 'http://tomaszgadek.com/api/students/';

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(this.url);
  }

  getDetails(index: string) {
    return this.http.get(this.url+index);
  }
}
