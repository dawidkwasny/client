import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit {

  private index: string;
  public details: any;
  constructor(private service: DataService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.index = this.route.snapshot.paramMap.get('id');
    this.getDetails();
  }

  getDetails() {
    this.service.getDetails(this.index).subscribe(response => {
      this.details = response;
    });
  }
  
  goBack() {
    this.router.navigate(['']);
  }
}
