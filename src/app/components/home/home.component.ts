import { Component, OnInit, HostListener } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public items: any;
  public headElements = ['Index number','Mark','Group','Lecture points','Homework points','All points','Presence','Absence'];
  public itemElements = ['index','mark','group','lecturePoints','homeworkPoints','allPoints','presenceCounter','absenceCounter'];
  constructor(private service: DataService, private router: Router) { }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.service.getAll().subscribe(response => {
      this.items = response;
    });
  }

  onSelect(item) {
    this.router.navigate(['/details',item.index]);
  }

  goTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  @HostListener('window:scroll')
  showTopButton() {
    if(document.body.scrollTop > 250 || document.documentElement.scrollTop > 250) {
      (document.querySelector(".scroll") as HTMLElement).style.display = 'block';
    } else {
      (document.querySelector(".scroll") as HTMLElement).style.display = 'none';
    }
  }

}
